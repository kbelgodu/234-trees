Run command java Tester for running the code.
Sample response as follows.

C:\Softwares\StudyMaterial\cs430\Assignments\Assignment7-Programming\classes>java Tester
Please enter number of records you want to insert [0 to quit]
10
INSERT :: The elapsed time was 8.60748E-4 seconds.
Inorder values are
1, 2, 9, 10, 36, 42, 46, 66, 71, 77,


Please enter number you want to delete
10
Deleting  10
DELETE :: The elapsed time was 2.8112E-5 seconds.
1, 2, 9, 36, 42, 46, 66, 71, 77,

Please enter number of records you want to insert [0 to quit]
0




Report contains insertion and deletion time for record count of 10,100,1000,10000,100000,1000000,10000000 records