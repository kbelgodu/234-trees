
/**
 * @author Karthik Belgodu
 *
 */
public final class Timer {

	static final String MESSAGE_1 = "The elapsed time was ";

	static final long NANO_FACTOR = 1000000000;  // nanoseconds per second

	static final String MESSAGE_2 = " seconds.";

	static long startTime,
	     finishTime;
	     
	static double elapsedTime;

	public static void start(){
		startTime = System.nanoTime();
		
	}
	
	public static void stop(){
		finishTime = System.nanoTime();
	}
	
	public static void timeDone(){
		elapsedTime = finishTime - startTime;
		Double time = (elapsedTime / NANO_FACTOR);
		System.out.println (MESSAGE_1 + (elapsedTime / NANO_FACTOR) + MESSAGE_2);

	}
}
