
/**
 * 
 * @author Karthik Belgodu
 *
 */
/*
 * node class which holds 3 keys and 4 values. 
 * this is the basic node structure in 2-3-4 Tree
 */

public class Node {

	//for holding the obj keys
	public Object[] obj = new Object[3];
	//maximum child nodes possible
	public Node[] childNodes = new Node[4];
	//boolean for handling leaf node
	public boolean isLeafNode;
	//for holding keys count
	public int numKeys = 0;
	//space for holding keys
	public int[] keys = new int[3];

	/*
	 * remove the right or left child
	 */
	void remove(int index, int leftOrRightChild) {
		if (index >= 0) {
			int i;
			for (i = index; i < numKeys - 1; i++) {
				keys[i] = keys[i + 1];
				obj[i] = obj[i + 1];
				if (!isLeafNode) {
					if (i >= index + leftOrRightChild) {
						childNodes[i] = childNodes[i + 1];
					}
				}
			}
			keys[i] = 0;
			obj[i] = null;
			if (!isLeafNode) {
				if (i >= index + leftOrRightChild) {
					childNodes[i] = childNodes[i + 1];
				}
				childNodes[i + 1] = null;
			}
			numKeys--;
		}
	}

	/*
	 * search in node for particular key
	 */
	int search(int key) {
		int leftIndex = 0;
		int rightIndex = numKeys - 1;

		while (leftIndex <= rightIndex) {
			final int middleIndex = leftIndex + ((rightIndex - leftIndex) / 2);
			if (keys[middleIndex] < key) {
				leftIndex = middleIndex + 1;
			} else if (keys[middleIndex] > key) {
				rightIndex = middleIndex - 1;
			} else {
				return middleIndex;
			}
		}

		return -1;
	}
	/*
	 * this method moves right by 1
	 */
	void shiftRightByOne() {
		if (!isLeafNode) {
			childNodes[numKeys + 1] = childNodes[numKeys];
		}
		for (int i = numKeys - 1; i >= 0; i--) {
			keys[i + 1] = keys[i];
			obj[i + 1] = obj[i];
			if (!isLeafNode) {
				childNodes[i + 1] = childNodes[i];
			}
		}
	}

	/*
	 * getting number of keys at particular node
	 */
	int subtreeRootNodeIndex(int key) {
		for (int i = 0; i < numKeys; i++) {
			if (key < keys[i]) {
				return i;
			}
		}
		return numKeys;
	}

}