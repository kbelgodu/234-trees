import java.util.Random;
import java.util.Scanner;


/**
 * @author Karthik Belgodu
 *
 */

public class Tester {

	public static void main(String[] args) {
		int nextInt = 0;

		while (true) {
			Tree24 tree = new Tree24();
			System.out
					.println("Please enter number of records you want to insert [0 to quit]");
			Scanner scan = new Scanner(System.in);
			Random random = new Random();

			int value = scan.nextInt();
			if (value == 0) {
				System.exit(0);
			}
			Timer.start();
			for (int i = 0; i < value; i++) {
				nextInt = random.nextInt(value*8);
				tree.add(nextInt, nextInt);
			}

			Timer.stop();
			System.out.print("INSERT :: ");
			Timer.timeDone();

			System.out.println("Inorder values are ");
			System.out.println(tree.toString());
			System.out.println();
			
			/*System.out.println();
			tree.treeInOrder(tree.getRoot());
*/
			System.out.println();
			
			System.out.println("Please enter number you want to delete");
			scan = new Scanner(System.in);
			nextInt = scan.nextInt();
			System.out.println("Deleting  " + nextInt);

			Timer.start();
			tree.delete(nextInt);
			Timer.stop();
			System.out.print("DELETE :: ");
			Timer.timeDone();

			System.out.println(tree.toString());
			System.out.println();

		}
	}
}