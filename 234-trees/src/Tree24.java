/**
 * @author Karthik Belgodu
 * 
 */
/*
 * This is the main class for handling tree operations. The operations handled
 * are insert and delete
 */

public class Tree24 {

	private Node root;

	static String result = "";

	public Tree24() {
		root = new Node();
		root.isLeafNode = true;
	}

	public Node getRoot() {
		return this.root;
	}

	// Inorder walk over the tree.
	// This will display the tree in order

	public void treeInOrder(Node node) {

		if (node == null) {
			return;
		}

		treeInOrder(node.childNodes[0]);
		for (int i = 0; i < node.numKeys; i++) {
			System.out.print(node.obj[i] + ", ");
		}
		treeInOrder(node.childNodes[1]);
		treeInOrder(node.childNodes[2]);
		treeInOrder(node.childNodes[3]);

	}

	/*
	 * This method is used to add new node values
	 * 
	 * @param key
	 * 
	 * @param object
	 */
	public void add(int key, Object object) {
		Node rootNode = root;
		// to check if we can input the values to node or not
		if (!update(root, key, object)) {
			if (rootNode.numKeys == 3) {
				Node newRootNode = new Node();
				root = newRootNode;
				newRootNode.isLeafNode = false;
				root.childNodes[0] = rootNode;
				// Split the child node if the keys are already full
				split(newRootNode, 0, rootNode);
				// Insert the key into the 2-3-4 Tree with root newRootNode.
				insert(newRootNode, key, object);
			} else {
				// Insert the key into the 2-3-4 Tree with root rootNode.
				insert(rootNode, key, object);
			}
		}
	}

	void split(Node parentNode, int i, Node node) {
		Node newNode = new Node();
		newNode.isLeafNode = node.isLeafNode;
		newNode.numKeys = 1;

		for (int j = 0; j < 1; j++) {
			newNode.keys[j] = node.keys[j + 2];
			newNode.obj[j] = node.obj[j + 2];
		}
		if (!newNode.isLeafNode) {
			for (int j = 0; j < 2; j++) {
				newNode.childNodes[j] = node.childNodes[j + 2];
			}
			for (int j = 2; j <= node.numKeys; j++) {
				node.childNodes[j] = null;
			}
		}
		for (int j = 2; j < node.numKeys; j++) {
			node.keys[j] = 0;
			node.obj[j] = null;
		}
		node.numKeys = 1;

		for (int j = parentNode.numKeys; j >= i + 1; j--) {
			parentNode.childNodes[j + 1] = parentNode.childNodes[j];
		}
		parentNode.childNodes[i + 1] = newNode;
		for (int j = parentNode.numKeys - 1; j >= i; j--) {
			parentNode.keys[j + 1] = parentNode.keys[j];
			parentNode.obj[j + 1] = parentNode.obj[j];
		}
		parentNode.keys[i] = node.keys[1];
		parentNode.obj[i] = node.obj[1];
		node.keys[1] = 0;
		node.obj[1] = null;
		parentNode.numKeys++;
	}

	// Insert an element into a 2-3-4 Tree. This is non full node hence value
	// will be inserted to child node
	void insert(Node node, int key, Object object) {
		int i = node.numKeys - 1;
		if (node.isLeafNode) {
			// Since node is not a full node insert the new element into its
			// proper place within node.
			while (i >= 0 && key < node.keys[i]) {
				node.keys[i + 1] = node.keys[i];
				node.obj[i + 1] = node.obj[i];
				i--;
			}
			i++;
			node.keys[i] = key;
			node.obj[i] = object;
			node.numKeys++;
		} else {
			while (i >= 0 && key < node.keys[i]) {
				i--;
			}
			i++;
			if (node.childNodes[i].numKeys == (3)) {
				split(node, i, node.childNodes[i]);
				if (key > node.keys[i]) {
					i++;
				}
			}
			insert(node.childNodes[i], key, object);
		}
	}

	/*
	 * this method id used to merge two nodes
	 */
	int merge(Node node, Node srcNode) {
		int medKeyIndex;
		if (srcNode.keys[0] < node.keys[node.numKeys - 1]) {
			int i;
			if (!node.isLeafNode) {
				node.childNodes[srcNode.numKeys + node.numKeys + 1] = node.childNodes[node.numKeys];
			}
			for (i = node.numKeys; i > 0; i--) {
				node.keys[srcNode.numKeys + i] = node.keys[i - 1];
				node.obj[srcNode.numKeys + i] = node.obj[i - 1];
				if (!node.isLeafNode) {
					node.childNodes[srcNode.numKeys + i] = node.childNodes[i - 1];
				}
			}

			medKeyIndex = srcNode.numKeys;
			node.keys[medKeyIndex] = 0;
			node.obj[medKeyIndex] = null;

			for (i = 0; i < srcNode.numKeys; i++) {
				node.keys[i] = srcNode.keys[i];
				node.obj[i] = srcNode.obj[i];
				if (!srcNode.isLeafNode) {
					node.childNodes[i] = srcNode.childNodes[i];
				}
			}
			if (!srcNode.isLeafNode) {
				node.childNodes[i] = srcNode.childNodes[i];
			}
		} else {
			medKeyIndex = node.numKeys;
			node.keys[medKeyIndex] = 0;
			node.obj[medKeyIndex] = null;

			int offset = medKeyIndex + 1;
			int i;
			for (i = 0; i < srcNode.numKeys; i++) {
				node.keys[offset + i] = srcNode.keys[i];
				node.obj[offset + i] = srcNode.obj[i];
				if (!srcNode.isLeafNode) {
					node.childNodes[offset + i] = srcNode.childNodes[i];
				}
			}
			if (!srcNode.isLeafNode) {
				node.childNodes[offset + i] = srcNode.childNodes[i];
			}
		}
		node.numKeys += srcNode.numKeys;
		return medKeyIndex;
	}

	/*
	 * This method is used to delete the key, along with that value as well
	 */

	public void delete(int key) {
		// calling delete method after passing the key and root element
		delete(root, key);
	}

	public void delete(Node node, int key) {
		if (node.isLeafNode) {
			int i;
			if ((i = node.search(key)) != -1) {
				node.remove(i, 0);
			}
		} else {
			int i;
			if ((i = node.search(key)) != -1) {
				Node leftChildNode = node.childNodes[i];
				Node rightChildNode = node.childNodes[i + 1];
				if (leftChildNode.numKeys >= 2) {
					Node predecessorNode = leftChildNode;
					Node erasureNode = predecessorNode;
					while (!predecessorNode.isLeafNode) {
						erasureNode = predecessorNode;
						predecessorNode = predecessorNode.childNodes[node.numKeys - 1];
					}
					node.keys[i] = predecessorNode.keys[predecessorNode.numKeys - 1];
					node.obj[i] = predecessorNode.obj[predecessorNode.numKeys - 1];
					delete(erasureNode, node.keys[i]);
				} else if (rightChildNode.numKeys >= 2) {
					Node successorNode = rightChildNode;
					Node erasureNode = successorNode;
					while (!successorNode.isLeafNode) {
						erasureNode = successorNode;
						successorNode = successorNode.childNodes[0];
					}
					node.keys[i] = successorNode.keys[0];
					node.obj[i] = successorNode.obj[0];
					delete(erasureNode, node.keys[i]);
				} else {
					int medianKeyIndex = merge(leftChildNode, rightChildNode);
					updateKey(node, i, 1, leftChildNode, medianKeyIndex);
					delete(leftChildNode, key);
				}
			} else {
				i = node.subtreeRootNodeIndex(key);
				Node childNode = node.childNodes[i]; 
				if (childNode.numKeys == 1) {
					Node leftChildSibling = (i - 1 >= 0) ? node.childNodes[i - 1]
							: null;
					Node rightChildSibling = (i + 1 <= node.numKeys) ? node.childNodes[i + 1]
							: null;
					if (leftChildSibling != null
							&& leftChildSibling.numKeys >= 2) {
						childNode.shiftRightByOne();
						childNode.keys[0] = node.keys[i - 1];
						childNode.obj[0] = node.obj[i - 1];
						if (!childNode.isLeafNode) {
							childNode.childNodes[0] = leftChildSibling.childNodes[leftChildSibling.numKeys];
						}
						childNode.numKeys++;

						node.keys[i - 1] = leftChildSibling.keys[leftChildSibling.numKeys - 1];
						node.obj[i - 1] = leftChildSibling.obj[leftChildSibling.numKeys - 1];

						leftChildSibling
								.remove(leftChildSibling.numKeys - 1, 1);
					} else if (rightChildSibling != null
							&& rightChildSibling.numKeys >= 2) {
						childNode.keys[childNode.numKeys] = node.keys[i];
						childNode.obj[childNode.numKeys] = node.obj[i];
						if (!childNode.isLeafNode) {
							childNode.childNodes[childNode.numKeys + 1] = rightChildSibling.childNodes[0];
						}
						childNode.numKeys++;

						node.keys[i] = rightChildSibling.keys[0];
						node.obj[i] = rightChildSibling.obj[0];
						rightChildSibling.remove(0, 0);
					} else { if (leftChildSibling != null) {
							int medianKeyIndex = merge(childNode,
									leftChildSibling);
							updateKey(node, i - 1, 0, childNode, medianKeyIndex);
						} else if (rightChildSibling != null) {
							int medianKeyIndex = merge(childNode,
									rightChildSibling);
							updateKey(node, i, 1, childNode, medianKeyIndex);
						}
					}
				}
				delete(childNode, key);
			}
		}
	}

	
	/**
	 * this method is used to search particular key
	 * 
	 * @param key
	 * @return
	 */
	public Object search(int key) {
		return search(root, key);
	}

	/*
	 * Printing the tree details
	 */
	String printTree(Node node) {
		String string = "";
		if (node != null) {
			if (node.isLeafNode) {
				for (int i = 0; i < node.numKeys; i++) {
					string += node.obj[i] + ", ";
				}
			} else {
				int i;
				for (i = 0; i < node.numKeys; i++) {
					string += printTree(node.childNodes[i]);
					string += node.obj[i] + ", ";
				}
				string += printTree(node.childNodes[i]);
			}
		}
		return string;
	}

	void updateKey(Node srcNode, int srcKeyIndex, int childIndex, Node destkey,
			int medianKeyIndex) {
		destkey.keys[medianKeyIndex] = srcNode.keys[srcKeyIndex];
		destkey.obj[medianKeyIndex] = srcNode.obj[srcKeyIndex];
		destkey.numKeys++;

		srcNode.remove(srcKeyIndex, childIndex);

		if (srcNode == root && srcNode.numKeys == 0) {
			root = destkey;
		}
	}

	/*
	 * For printing the result (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return printTree(root);
	}

	// Recursive search method. This methd will search based on the key we
	// present
	public Object search(Node node, int key) {
		int i = 0;
		while (i < node.numKeys && key > node.keys[i]) {
			i++;
		}
		if (i < node.numKeys && key == node.keys[i]) {
			return node.obj[i];
		}
		if (node.isLeafNode) {
			return null;
		} else {
			return search(node.childNodes[i], key);
		}
	}

	/*
	 * this method is used to check if the node is leaf node and which node to
	 * update for keys
	 */
	private boolean update(Node node, int key, Object object) {
		while (node != null) {
			int i = 0;
			while (i < node.numKeys && key > node.keys[i]) {
				i++;
			}
			if (i < node.numKeys && key == node.keys[i]) {
				node.obj[i] = object;
				return true;
			}
			if (node.isLeafNode) {
				return false;
			} else {
				node = node.childNodes[i];
			}
		}
		return false;
	}
}
